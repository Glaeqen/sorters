#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define VOLUME 25
#define RANGE 10

int initRand = 0;

void generalTest();
//Management functions
void allocArray(int **);
void allocArrayV(int **, int);
void genRandArray(int *);
void genRandArrayV(int *, int);
void arrayCopier(int *, int *);
void arrayCopierV(int *, int *, int);
void printArray(int *);
void printArrayV(int *, int);
void swap(int *, int *);
//Sorters
//BubbleSort
void bubbleSort(int *);
float timerBubbleSort(int *);
//MergeSort
void merge(int *, int, int, int);
void mergeSort(int *, int, int);
float timerMergeSort(int *);
//QuickSort Hoare Scheme
int partitionHoare(int *, int, int, int *, int *);
void quickSortH(int *, int, int, int *, int *);
float timerQuickSortH(int *, int * , int *);
//QuickSort Lomuto Scheme
int partitionLomuto(int *, int, int, int *, int *);
void quickSortL(int *, int, int, int *, int *);
float timerQuickSortL(int *, int *, int *);
//HeapSort
int left(int);
int right(int);
void heapSort(int *, int, int *, int *);
void maxHeapify(int *, int, int, int *, int *);
float timerHeapSort(int *, int *, int *);
//CountingSort
void countingSort(int *, int, int);


int main(int argc, char **argv){
	int *array;
	allocArray(&array);
	genRandArray(array);
  printArray(array);
  countingSort(array, VOLUME, RANGE);
  printArray(array);

//	int copyCounter = 0;
	//int compareCounter = 0;
//	float tHS = timerHeapSort(array, &copyCounter, &compareCounter);
//	printf("Done. Time: %0.3fs; CpCntr: %d; CmpCntr: %d\n", tHS, copyCounter, compareCounter);
	free(array);
  return 0;
}

void generalTest(){
  
}

void allocArray(int **arr){
	*arr = (int*) malloc(VOLUME * sizeof(*arr));
}
void allocArrayV(int **arr, int volume){
	*arr = (int*) malloc(volume * sizeof(*arr));
}
void genRandArray(int *arr){
	if(!initRand){
		srand(time(NULL));
	}
	initRand = 1;
	for(int i = 0; i<VOLUME; i++){
		*(arr+i)=rand()%RANGE;
	}
}
void genRandArrayV(int *arr, int volume){
	if(!initRand){
		srand(time(NULL));
	}
	initRand = 1;
	for(int i = 0; i<volume; i++){
		*(arr+i)=rand()%RANGE;
	}	
}
void arrayCopier(int *arr, int *arrayCopy){
	for(int i=0; i<VOLUME; i++){
		*(arrayCopy+i)=*(arr+i);
	}
}
void arrayCopierV(int *arr, int *arrayCopy, int volume){
	for(int i=0; i<volume; i++){
		*(arrayCopy+i)=*(arr+i);
	}
}
void printHeap(int *arr, int volume){
    int tmpVol = volume;
    int counter = 0;
    while(tmpVol){
        counter++;
        tmpVol/=2;
    }
    int spacesAmount = 0;
    for(int i=0; i<counter; i++){
		spacesAmount=spacesAmount*2+1;
    }
    int k = 0;
    for(int i=0; i<counter; i++){
        for(int j=0; j<spacesAmount/2; j++){
			printf(" ");
        }
        int tmpLength = pow(2, i);
        for(int j=0; j<tmpLength; j++){ 
			if(k<volume){
				printf("%d ", arr[k++]);
				for(int k=0; k<spacesAmount-1; k++){
					printf(" ");
				}
			}
		}
		spacesAmount/=2;
		printf("\n");
    }
}
void printArray(int *arr){
	for(int i=0; i<VOLUME; i++){
		printf("%d ", *(arr+i));
		if(i&&!((i+1)%10))	printf("\n");	
	}
	printf(" end\n");
}
void printArrayV(int *arr, int volume){
  for(int i=0; i<volume; i++){
		printf("%d ", *(arr+i));
		if(i&&!((i+1)%10))	printf("\n");	
	}
	printf(" end\n");
}
void swap(int *a, int *b){
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void bubbleSort(int *arr){
	for(int i = 0; i<=VOLUME-2; i++){
		for(int j = 1; j<=VOLUME-1-i; j++){
			if(*(arr+j)>*(arr+j-1)){
				swap((arr+j), (arr+j-1));
			}
		}
	}
}
float timerBubbleSort(int *arr){
  clock_t start = clock();
  bubbleSort(arr);
  clock_t end = clock();
  return (float)(end-start)/CLOCKS_PER_SEC;
}

void merge(int *arr, int start, int mid, int end){
	int *left;
	int *right;
	allocArrayV(&left, mid-start+1);
	allocArrayV(&right, end-mid);
	for(int i=0; i<end-mid; i++){
		*(left+i)=*(arr+start+i);
		*(right+i)=*(arr+i+mid+1);
	}
	if((end-start+1)%2){  
		*(left+mid-start)=*(arr+mid);
	}
	int leftCounter = 0;
	int rightCounter = 0;
	int mainCounter = 0;
	while(mainCounter<(end-start+1) && leftCounter<(mid-start+1) && rightCounter<(end-mid)){
		if(*(left+leftCounter)<*(right+rightCounter)){
			*(arr+start+mainCounter) = *(left+leftCounter);
			leftCounter++;
		}
		else{
			*(arr+start+mainCounter) = *(right+rightCounter);
			rightCounter++;
		}
		mainCounter++;
	}
	while(leftCounter<(mid-start+1)){
		*(arr+start+mainCounter) = *(left+leftCounter);
		mainCounter++;
		leftCounter++;
	}
	while(rightCounter<(end-mid)){
		*(arr+start+mainCounter) = *(right+rightCounter);
		mainCounter++;
		rightCounter++;
	}
	free(left);
	free(right);
}
void mergeSort(int *arr, int start, int end){
	if(start<end){
		int mid = (start+end)/2;
		mergeSort(arr, start, mid);
		mergeSort(arr, mid+1, end);
		merge(arr, start, mid, end);
	}
}
float timerMergeSort(int *arr){
	clock_t start = clock(); 
	mergeSort(arr, 0, VOLUME-1);
	clock_t end = clock();
	float time = (float)(end-start)/CLOCKS_PER_SEC;
	return time;
}

int partitionHoare(int *arr, int start, int end, int *copyCounter, int *compareCounter){
	int pivot = arr[start];
	int left = start-1;
	int right = end+1;
	//printf("Function, array:\n");
	//printArray(arr);
	while(1){
		do{
			(*compareCounter)++;
			//if(right<=end)	printf("%d", arr[right]);
			right--;
			//printf("->%d\n", arr[right]);
		}while(arr[right]>pivot);
		//printf("right/T[right]= %d/%d;\n", right, arr[right]);
		do{
			(*compareCounter)++;
			//if(left>=start)	printf("%d", arr[left]);
			left++;
			//printf("->%d\n", arr[left]);
		}while(arr[left]<pivot);			
		//printf("left/T[left]= %d/%d;\n", left, arr[left]);
		if(left<right){
			//printf("Before swap:\n");
			//printArray(arr);
			//printf("pivot= %d;\n", pivot);
			swap(&arr[left], &arr[right]);
			(*copyCounter)++;
			//printf("After swap:\n");
			//printArray(arr);
		}
		else{
			//printf("return right/T[right] = %d/%d\n\n", right, arr[right]);
			return right;
		}
	}
}
void quickSortH(int *arr, int start, int end, int *copyCounter, int *compareCounter){
	if(start<end){
		int mid = partitionHoare(arr, start, end, copyCounter, compareCounter);
		quickSortH(arr, start, mid, copyCounter, compareCounter);
		quickSortH(arr, mid+1, end, copyCounter, compareCounter);
	}
}
float timerQuickSortH(int *arr, int *copyCounter, int *compareCounter){
	clock_t start = clock();
	quickSortH(arr, 0, VOLUME-1, copyCounter, compareCounter);
	clock_t end = clock();
	return (float)(end-start)/CLOCKS_PER_SEC;
}

int partitionLomuto(int *arr, int start, int end, int *copyCounter, int *compareCounter){
	//printf("Starting array:\n");
	//printArray(arr);
	int pivot = arr[end];
	int left = start-1;
	//printf("start= %d, end= %d\n", start, end);
	//printf("pivot= %d, left= %d\n", pivot, left);
	for(int i=start; i<=end-1; i++){
		(*compareCounter)++;
		//printf("Is arr[%d]=%d<=pivot/%d/\n", i, arr[i], pivot);
		if(arr[i]<=pivot){
			//printf("Yup. Left=%d>", left);
			left++;
			//printf(">%d\n", left);
			//printArray(arr);
			//printf("swap arr[%d]=%d<=>arr[%d]=%d\n", left, arr[left], i, arr[i]);
			swap(&arr[left], &arr[i]);
			//printf("swapped arr[%d]=%d<=>arr[%d]=%d\n", left, arr[left], i, arr[i]);
			//printArray(arr);
			(*copyCounter)++;
		}
	}
	//printArray(arr);
	//printf("swap arr[%d]=%d<=>arr[%d]=%d\n", left+1, arr[left+1], end, arr[end]);
	swap(&arr[left+1], &arr[end]);
	//printf("swapped arr[%d]=%d<=>arr[%d]=%d\n", left+1, arr[left+1], end, arr[end]);
	//printArray(arr);
	(*copyCounter)++;
	//printf("return left+1=%d\n", left+1);
	return left+1;
}
void quickSortL(int *arr, int start, int end, int *copyCounter, int *compareCounter){
	if(start<end){
		int mid = partitionLomuto(arr, start, end, copyCounter, compareCounter);
		quickSortL(arr, start, mid-1, copyCounter, compareCounter);
		quickSortL(arr, mid+1, end, copyCounter, compareCounter);
	}
}
float timerQuickSortL(int *arr, int *copyCounter, int *compareCounter){
	clock_t start = clock();
	quickSortL(arr, 0, VOLUME-1, copyCounter, compareCounter);
	clock_t end = clock();
	return (float)(end-start)/CLOCKS_PER_SEC;
}

int left(int i){
	return 2*i+1;
}
int right(int i){
	return 2*i+2;
}
void heapSort(int *arr, int volume, int *copyCounter, int *compareCounter){
	int heapSize = volume-1;
	for(int i=(volume-1)/2; i>=0; i--){
		maxHeapify(arr, i, heapSize, copyCounter, compareCounter);
	}
	for(int i=volume-1; i>=1; i--){
		swap(&arr[0], &arr[i]);
		(*copyCounter)++;
		heapSize--;
		maxHeapify(arr, 0, heapSize, copyCounter, compareCounter);
	}
}
void maxHeapify(int *arr, int i, int heapSize, int *copyCounter, int *compareCounter){
	int l = left(i);
	int r = right(i);
	int largest;
	if((l<=heapSize)&&(arr[l]>arr[i])){
		(*compareCounter)++;
		largest = l;
	}
	else{
		(*compareCounter)++;
		largest = i;
	}
	if((r<=heapSize)&&(arr[r]>arr[largest])){
		(*compareCounter)++;
		largest = r;
	}
	if(largest!=i){
		(*compareCounter)++;
		swap(&arr[largest], &arr[i]);
		(*copyCounter)++;
		maxHeapify(arr, largest, heapSize, copyCounter, compareCounter);
	}
}
float timerHeapSort(int *arr, int *copyCounter, int *compareCounter){
	clock_t start = clock();
	heapSort(arr, VOLUME, copyCounter, compareCounter);
	clock_t end = clock();
	return (float)(end-start)/CLOCKS_PER_SEC;
}

void countingSort(int *arr, int volume, int numbersRange){
	int *arrSorted;
	int *arrTmp;
	allocArrayV(&arrSorted, volume);
	allocArrayV(&arrTmp, numbersRange);
	for(int i=0; i<numbersRange; i++){
		arrTmp[i]=0;
	}
	for(int i=0; i<volume; i++){
		arrTmp[arr[i]]++;
	}
	for(int i=1; i<numbersRange; i++){
		arrTmp[i]+=arrTmp[i-1];
	}
  for(int i=volume-1; i>=0; i--){
    arrSorted[arrTmp[arr[i]]]=arr[i];
    arrTmp[arr[i]]--;
  }
  for(int i=0; i<volume; i++){
    arr[i]=arrSorted[i];
  }
  free(arrSorted);
  free(arrTmp);
}
